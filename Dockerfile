FROM nginx:alpine

EXPOSE 443

COPY ./nginx.conf /etc/nginx/templates/default.conf.template
