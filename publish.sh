dotnet publish -f netcoreapp3.1 -c Release -r linux-arm

# test $? -ne 0 || exit

pushd ./karaokemaster-web

yarn install
yarn build

# test $? -ne 0 || exit

popd

rm -f KaraokeMaster.zip

mkdir ./publish
mkdir ./publish/api
mkdir ./publish/web

cp -r ./KaraokeMaster.Api/bin/Release/netcoreapp3.1/linux-arm/publish/* ./publish/api/
cp -r ./karaokemaster-web/build/* ./publish/web/

pushd publish

zip -r ../KaraokeMaster.zip .

popd

rm -rf publish

scp ./KaraokeMaster.zip karaoke.cheznorah.com:~/
# ssh karaoke.cheznorah.com 'sudo ./install_KaraokeMaster.sh'
