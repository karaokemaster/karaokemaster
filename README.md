# KarokeMaster

KaraokeMaster grew out of my frustration with the other options out there for running a YouTube-based karaoke "machine".
It is written in C# using ASP.NET Core 2.1, with the intent of running on ARM Linux (specifically, a Raspberry Pi).

## License

KaraokeMaster is free software, provided under GPLv3.

## Environment

In my personal environment, I am actually using two Raspberry Pi computers to run KaraokeMaster. The first is a Pi server,
running headless. It is hosting the KarokeMaster web site, and is configured with an easily-accessible hostname on my 
network. The second is a Pi with an upgraded DAC, used to display the KaraokeMaster player interface in full-screen kiosk
mode.

